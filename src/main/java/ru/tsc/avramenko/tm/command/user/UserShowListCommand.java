package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.User;

import java.util.List;

public class UserShowListCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        System.out.println("List of users.");
        @Nullable final List<User> users = serviceLocator.getUserService().findAll();
        if (users == null) throw new ProcessException();
        int index = 1;
        for (final User user : users) {
            System.out.println("- - - № " + index + " - - -");
            showUser(user);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}